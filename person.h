/*
 * person.h
 *
 *  Created on: Mar 2, 2020
 *      Author: fernanda
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <sys/queue.h>

#define MALLOC(ptr,size) 	\
	do {					\
		ptr=malloc(size); 	\
		if (!ptr) 			\
			abort(); 		\
	} while(0)

#define FREE(ptr) 			\
	do {					\
		free(ptr);			\
		ptr=NULL;			\
	} while(0)
		
#define STRICT
#define READ_MODE "r"
#define DELIMITER ";"

enum PersonState {
	SINGLE=0,
	MARRIED=1,
	DIVORCED=2,
	WIDOW=3,
	NOT_VALID = 4
};

enum Ordering {
	ASCENDING,
	DESCENDING
};

struct Person {
	char *name;
	int age;
	enum PersonState state;
	char *died;
	TAILQ_ENTRY(Person) next;
};

int person_init();

int person_deinit();

int person_load_from_file(const char *name);

struct Person* person_create(char *name, int age, int statement, char *died);

struct Person *person_process_file_line(char *line);

int person_add(struct Person *person);

struct Person *person_remove_head();

char *person_get_state(enum PersonState state);

void person_print_a_person(struct Person *person);

void person_print_all(struct Person *from);

struct Person *person_find_by_name(const char *name, struct Person *from);

struct Person *person_find_by_age(int age, struct Person *from);

struct Person *person_find_by_state(enum PersonState state, struct Person *from);

struct Person *person_find_by_died(const char *died, struct Person *from);

struct Person *person_first();

struct Person *person_last();

void person_bubble_sort(enum Ordering ordering,
			int (*comparer)(const struct Person *a, const struct Person *b));

int person_compare_by_name(const struct Person *a, const struct Person *b);

int person_compare_by_age(const struct Person *a, const struct Person *b);

int person_compare_by_state(const struct Person *a, const struct Person *b);

int person_compare_by_died(const struct Person *a, const struct Person *b);

#endif /* PERSON_H_ */