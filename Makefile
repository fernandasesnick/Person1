#Makefile for perosn application
#listing the rules for building the executable the file should be named 'Makefile' or 'makefile'. 

#1st default target

BIN=bin
SRC = src
CFLAGS = -Wall -std=c99 -g -pedantic

$(BIN)/person:$(BIN)/person.o $(BIN)
#calling the c compiler does the person.o exist?
		$(CC) -o $(BIN)/person $(BIN)/person.o
		
$(BIN)/.%o:$(SRC)/.%c
		$(CC) -c $(SCR)/$< -o $(BIN)/$@ $(CFLAGS)
		
$(BIN):
		mkdir $(BIN)
clean:
	$(RM) $(BIN)
		